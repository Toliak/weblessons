sudo ln -sf /home/box/weblessons/web   /home/box/web

if [ -f "/home/box/web/etc/nginx$1.conf" ]; then
    sudo ln -sf "/home/box/web/etc/nginx$1.conf" "/etc/nginx/sites-enabled/test$1.conf"
else
    echo "/home/box/web/etc/nginx$1.conf not found"
fi

sudo rm /etc/nginx/sites-enabled/default
sudo /etc/init.d/nginx restart

if [ -f "/home/box/web/etc/gunicorn$1.conf" ]; then
    sudo ln -sf "/home/box/web/etc/gunicorn$1.conf" "/etc/gunicorn.d/test$1.conf"
else
    echo "/home/box/web/etc/gunicorn$1.conf not found"
fi
sudo /etc/init.d/gunicorn restart

sudo /etc/init.d/mysql start

mysql -uroot -e "CREATE USER 'toliak'@'localhost' IDENTIFIED BY '123456';\
CREATE DATABASE stepik_web;\
GRANT ALL PRIVILEGES ON *.* TO 'toliak'@'localhost';"

/home/box/web/ask/manage.py makemigrations
/home/box/web/ask/manage.py makemigrations qa
/home/box/web/ask/manage.py migrate

cd /home/box/web/ask
# gunicorn --bind 127.0.0.1:8080 ask.wsgi