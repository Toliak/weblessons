cmake_minimum_required(VERSION 3.13)
project(WebServer)

set(CMAKE_CXX_STANDARD 14)

SET(CMAKE_CXX_FLAGS -pthread)

add_executable(WebServer main.cpp)