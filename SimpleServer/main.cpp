#include <iostream>
#include <cstring>
#include <thread>
#include <mutex>
#include <stack>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <thread>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

int server()
{
    sockaddr_in bindAddress{};
    bindAddress.sin_family = AF_INET;
    bindAddress.sin_port = htons(2222);
    bindAddress.sin_addr.s_addr = inet_addr("0.0.0.0");

    int socketId = socket(AF_INET, SOCK_STREAM, 0);
//    setsockopt(socketId, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, )
    bind(socketId, (struct sockaddr *) &bindAddress, sizeof(bindAddress));

    listen(socketId, 1);

    std::stack<std::thread *> threads;

    while (true) {
        std::cout << "Listening" << std::endl;
        int sock = accept(socketId, nullptr, nullptr);

        std::thread *thread = new std::thread([sock]() {
            while (true) {

                static const size_t MESSAGE_LENGTH = 1024;
                char message[MESSAGE_LENGTH] = {0};
                long messageLength = recv(sock, message, MESSAGE_LENGTH, 0);

                static std::mutex mutex;
                mutex.try_lock();
                std::cout << "[" << std::this_thread::get_id() << "]" << " Received " << messageLength << " bytes. Message: " << message << std::endl;

                if (strcmp(message, "close") == 0) {
                    std::cout << "[" << std::this_thread::get_id() << "]" << " Closing connection" << std::endl;
                    break;
                }

                send(sock, message, sizeof(char) * messageLength, 0);
                mutex.unlock();
            }

            close(sock);
        });

        threads.push(thread);
    }
}

int client()
{
    sockaddr_in connectionAddress{};
    connectionAddress.sin_family = AF_INET;
    connectionAddress.sin_port = htons(2222);
    connectionAddress.sin_addr.s_addr = inet_addr("127.0.0.1");

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    int state = connect(sock, (struct sockaddr *)&connectionAddress, sizeof(connectionAddress));
    std::cout << "Connection state: " << state << std::endl;

    while (true) {
        std::cout << "Enter message: ";
        std::string message;
        std::getline(std::cin, message);
        send(sock, message.c_str(), sizeof(char) * message.size(), 0);

        char receive[1024] = {0};
        ssize_t read = recv(sock, receive, sizeof(char) * 1024, 0);

        std::cout << "Received message: " << receive << std::endl;
    }
}

#pragma clang diagnostic pop

int main(int argc, const char *argv[])
{
    if (argc < 2) {
        return 1;
    } else {
        if (strcmp(argv[1], "0") == 0) {
            return server();
        } else if (strcmp(argv[1], "1") == 0) {
            return client();
        }
    }

}