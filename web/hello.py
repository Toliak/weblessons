from urllib.parse import parse_qs

def application(environ, start_response):
    query = environ['QUERY_STRING'].split('&')

    status = '200 OK'
    output = ''
    for q in query:
        output += q + '\n'

    response_headers = [('Content-type','text/plain')]
    start_response(status, response_headers)
    return [output.encode('utf-8')]