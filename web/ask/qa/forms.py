from django import forms
from django.contrib.auth import forms as auth_forms, authenticate
from django.contrib.auth.models import User

from qa.models import Answer, Question

auth_forms.UserCreationForm


class AskForm(forms.Form):
    title = forms.CharField(max_length=100)
    text = forms.CharField()
    user = None

    def __init__(self, *args, **kwargs):
        self.user = kwargs['user']
        del kwargs['user']
        super(AskForm, self).__init__(*args, **kwargs)

    def save(self):
        question = Question.objects.create(
            text=self.cleaned_data['text'],
            title=self.cleaned_data['title'],
            author=self.user
        )
        return question


class AnswerForm(forms.Form):
    text = forms.CharField()
    question = forms.IntegerField()
    user = None

    def __init__(self, *args, **kwargs):
        self.user = kwargs['user']
        del kwargs['user']
        super(AnswerForm, self).__init__(*args, **kwargs)

    def clean_question(self):
        question = self.cleaned_data['question']
        try:
            int(question)
        except ValueError:
            raise forms.ValidationError(u'Unexpected error. Please reload', code=1)
        return question

    def save(self):
        answer = Answer.objects.create(
            text=self.cleaned_data['text'],
            question_id=int(self.cleaned_data['question']),
            author=self.user
        )
        return answer


class SignupForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())

    def save(self):
        return User.objects.create_user(
            username=self.cleaned_data['username'],
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password']
        )


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
    user = None

    def clean(self):
        self.user = authenticate(
            username=self.cleaned_data['username'],
            password=self.cleaned_data['password']
        )
        if self.user is None:
            raise forms.ValidationError(u'Username or password is wrong')
