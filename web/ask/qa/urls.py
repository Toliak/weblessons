from django.conf.urls import url

import qa.views as qa_views

urlpatterns = [
    url(r'^$', qa_views.PageView.as_view(), name='new'),
    url(r'^login/$', qa_views.LoginView.as_view(), name='login'),
    # url(r'^logout/$', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^signup/$', qa_views.SignupView.as_view(), name='signup'),
    url(r'^ask/$', qa_views.AskView.as_view(), name='ask'),
    url(r'^popular/$', qa_views.PageView.as_view(), name='popular'),
    url(r'^new/$', qa_views.PageView.as_view(), name='new2'),
    url(r'^question/(?P<id>\d+)/$', qa_views.QuestionView.as_view(), name='question'),
]
