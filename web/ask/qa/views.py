from django.contrib.auth import login
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import resolve, reverse
from django.views.generic import TemplateView, FormView

from qa.forms import AskForm, AnswerForm, SignupForm, LoginForm
from qa.models import Answer, Question


class SignupView(FormView):
    template_name = "signup.html"
    form_class = SignupForm

    def form_invalid(self, form):
        return super(SignupView, self).form_invalid(form)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        self.success_url = reverse('new')
        return super(SignupView, self).form_valid(form)


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm

    def form_invalid(self, form):
        return super(LoginView, self).form_invalid(form)

    def form_valid(self, form):
        login(self.request, form.user)
        self.success_url = reverse('new')
        return super(LoginView, self).form_valid(form)


class QuestionView(FormView):
    template_name = "question.html"
    form_class = AnswerForm

    def get_context_data(self, **kwargs):
        question_id = int(self.request.resolver_match.kwargs['id'])
        question = get_object_or_404(Question, id=question_id)
        kwargs['question'] = question
        kwargs['answers'] = Answer.objects.filter(question=question)
        return super(QuestionView, self).get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super(QuestionView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_invalid(self, form):
        return super(QuestionView, self).form_invalid(form)

    def form_valid(self, form):
        form.save()

        question = Question.objects.get(id=form.cleaned_data['question'])
        self.success_url = question.get_url()
        return super(QuestionView, self).form_valid(form)


class PageView(TemplateView):
    template_name = "page.html"
    limit = 10

    def get(self, request, *args, **kwargs):
        url_name = resolve(request.path_info).url_name
        questions = Question.objects.new() if url_name == 'new' else Question.objects.popular()
        try:
            page = int(request.GET['page'])
        except (ValueError, KeyError):
            page = 1
        paginator = Paginator(questions, self.limit)

        kwargs['page'] = paginator.page(page)
        return super(PageView, self).get(request, *args, **kwargs)


class AskView(FormView):
    template_name = "ask.html"
    form_class = AskForm

    def post(self, request, *args, **kwargs):
        self.get_form()
        return super(AskView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(AskView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        question = form.save()
        self.success_url = question.get_url()
        return super(AskView, self).form_valid(form)


def just_ok(request):
    return HttpResponse('OK')
